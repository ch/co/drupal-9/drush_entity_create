<?php

namespace Drupal\drush_entity_create\Drush\Commands;

use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drush commands to create entitites.
 */
final class DrushEntityCreateCommands extends DrushCommands {

  /**
   * Constructs a DrushEntityCreateCommands object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly EntityTypeBundleInfo $bundleInfoManager,
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * Command description here.
   */
  #[CLI\Command(name: 'entity:create')]
  #[CLI\Argument(name: 'entity_type', description: 'Entity type to create')]
  #[CLI\Argument(name: 'entity_title', description: 'Entity title')]
  #[CLI\Option(name: 'bundle', description: 'Bundle')]
  #[CLI\Usage(name: 'entity:create node "Hello World" --bundle page', description: 'Create an entity')]
  public function createEntity(string $entity_type, string $entity_title, array $options = ['bundle' => NULL]) {

    $available_entity_types = array_keys($this->entityTypeManager->getDefinitions());

    $bundle = $options['bundle'];

    if (!in_array($entity_type, $available_entity_types)) {
      throw new \InvalidArgumentException("Unknown entity type: $entity_type");
    }

    $available_bundles = array_keys($this->bundleInfoManager->getBundleInfo($entity_type));
    $this->logger->info(print_r($available_bundles, TRUE));

    if ($bundle && !in_array($bundle, $available_bundles)) {
      throw new \InvalidArgumentException("Unknown bundle $bundle for entity type: $entity_type");
    }

    $entityStorage = $this->entityTypeManager->getStorage($entity_type);

    if ($this->entityTypeManager->getDefinition($entity_type)->hasKey('bundle')) {
      $values = [
        'type' => $bundle,
      ];
    }
    else {
      $values = [];
    }

    $new_entity = $entityStorage->create($values);
    $new_entity->setTitle($entity_title);
    $new_entity->save();

    $this->io()->info("created $entity_type");
  }

}
