<?php

namespace Drupal\Tests\drush_entity_create\Functional;

use Drupal\Tests\BrowserTestBase;
use Drush\TestTraits\DrushTestTrait;

/**
 * Tests of drush entity:create commmand.
 *
 * @group drush_entity_create
 */
class EntityCreateCommandTest extends BrowserTestBase {

  use DrushTestTrait;

  /**
   * Modules required by these tests.
   *
   * @var array
   */
  public static $modules = ['node', 'drush_entity_create'];

  /**
   * Theme to use.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Content type for running tests.
   *
   * @var \Drupal\node\Entity\NodeType
   */
  protected $contentType;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a content type.
    $this->contentType = $this->drupalCreateContentType();
  }

  /**
   * Test outcome when trying to create an unknown entity.
   */
  public function testCreateInvalidEntityTypeShouldFail() {
    // Test should exit status 1, and with nothing on stdout.
    $expected_status = 1;

    $this->drush('entity:create', ['missingentity', 'Entity title'], [], NULL, NULL, $expected_status);
    $this->assertOutputEquals('');
  }

  /**
   * Test creating a valid node entity.
   */
  public function testCreateValidNode() {
    // Test should exit status 0, and with nothing on stderr.
    $expected_status = 0;
    $options = ['bundle' => $this->contentType->id()];
    $node_title = $this->randomString(8);

    $this->drush('entity:create', ['node', $node_title], $options, NULL, NULL, $expected_status);
    $this->assertErrorOutputEquals('');

    $node = $this->drupalGetNodeByTitle($node_title);

    $this->assertNotNull($node, 'Load node created by drush entity:create');
  }

}
