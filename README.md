At the time of writing, drush provides commands like:

- `drush entity:save`
- `drush entity:delete`

but no corresponding:

- `drush entity:create`

This module implements entity:create, though it might not work for all
entities. It has been tested for the simple case of

```
drush entity:create node "Example node title" --bundle page
```

but not for other cases.
